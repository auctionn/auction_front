/* eslint-disable no-undef,no-else-return */
import React, { Component } from 'react';
// import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import getList from '../../utils/utilRcords';
// import getSearchList from '../../utils/utilSearchRcords';
import DaumMap from '../page-module/DaumMapList';
import FilterHeader from '../page-module/FilterHeader';
import List from '../page-module/List';
// import DetailSearch from '../page-module/DetailSearch';

class Home extends Component {
	constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
	}
    // componentWillUpdate(nextProps) {
	// 	console.log('componentWillUpdate: ', ' ', nextProps);
	// }
    // componentDidUpdate(prevProps, prevState) {
    //     console.log('componentDidUpdate: ', prevProps, ' ', prevState);
    // }
    shouldComponentUpdate(nextProps) {
        return JSON.stringify(nextProps) !== JSON.stringify(this.props);
    }
	onSearch(args) {
		getList(args).then((request) => {
			this.props.reLoad(request.data);
		});
	}
	goDetailPage() {
		// this.props.history.push(`/post/${id}`);
	}

	render() {
		return (
			<div className="container">
				<FilterHeader
					onSearch={this.onSearch}
				/>
				<div className="row contents-section clearfix">
					<div className="col-8 list-section">
						<List
							records={this.props.records}
							foundRecord={this.props.foundRecord}
							currentPage={this.props.currentPage}
							recordPerPage={24}
							query={this.props.query}
							searchFlag={this.props.searchFlag}
							onChange={this.onSearch}
							goDetail={this.goDetailPage}
							// status={this.state.status}
						/>
					</div>
					<div className="col-4 map-section">
						<DaumMap
							lat={this.props.lat}
							lng={this.props.lng}
							records={this.props.records}
							onChange={this.onSearch}
						/>
					</div>
				</div>
			</div>
		);
	}
}


Home.propTypes = {
	records: PropTypes.array.isRequired,
	foundRecord: PropTypes.number.isRequired,
	currentPage: PropTypes.number.isRequired,
	reLoad: PropTypes.func.isRequired,
	lat: PropTypes.number.isRequired,
	lng: PropTypes.number.isRequired,
	query: PropTypes.string.isRequired,
    searchFlag: PropTypes.string.isRequired,
};

function reload(args) {
	return {
		type: '',
		records: args && args.records ? args.records : [],
		foundRecord: args && args.foundRecord ? args.foundRecord : 0,
		currentPage: args && args.currentPage ? args.currentPage : 1,
		hq: args && args.hq ? args.hq : 1,
		query: args && args.query ? args.query : '',
        searchFlag: '',
    };
}

const listStateToProps = function (state) {
	return {
		records: state.listBox.records,
		foundRecord: state.listBox.foundRecord,
		currentPage: state.listBox.currentPage,
		lat: state.listBox.lat,
		lng: state.listBox.lng,
		hq: state.listBox.hq,
		query: state.listBox.query,
        searchFlag: state.listBox.searchFlag,
	};
};

const listDispatchToProps = dispatch => ({
	reLoad: args => dispatch(reload(args)),
});

Home = connect(listStateToProps, listDispatchToProps)(Home);

export default Home;
