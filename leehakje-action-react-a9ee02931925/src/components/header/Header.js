/* eslint-disable prefer-arrow-callback */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import logo from '../assets/images/logo.png';
// import getSearchMapXY from '../../utils/utilSearchRcord';
import getAutoComplete from '../../utils/utillAutoComplete';
import AutoComplete from './AutoComplete';
import getSearch from '../../utils/utilSearch';
// import getList from '../../utils/utilRcords';

let object = null;

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			keyword: '',
			autoCompleteState: false,
			regionAutoKeyword: [],
			courtAutoKeyword: [],
			courtIdAutoKeyword: [],
		};
		object = this;
		this.handleChange = this.handleChange.bind(this);
		this.keyUp = this.keyUp.bind(this);
		this.testHandle = this.testHandle.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.handleFocusOut = this.handleFocusOut.bind(this);
		this.handleFocusIn = this.handleFocusIn.bind(this);
	}
	static contextTypes = {
		router: PropTypes.object,
	}
	testHandle(event) {
		event.preventDefault();
		object.context.router.history.push('/popup');
	}
	handleChange(event) {
		this.setState({ keyword: event.target.value, enter: false });
	}
	// 주소 키워드를 입력하면
	// 그 키워드에 해당하는 좌표를 반환하여 지도맵에 띄운다.
	handleClick(item) {
		console.log('click', item)
		getSearch(item).then((request) => {
			object.props.reLoad(request.data);
		})
		this.setState({
			keyword: item,
			autoCompleteState: false,
		})
	}
	handleFocusOut() {
		setTimeout(() => {
			this.setState({
				autoCompleteState: false,
			})
		}, 100)
	}
	handleFocusIn() {
		this.setState({
			autoCompleteState: true,
		})
	}
	keyUp(event) {
		if (event.key === 'Escape') {
			this.setState({
				autoCompleteState: false,
			})
			return;
		}
		if (this.state.keyword === '') {
			this.setState({
				regionAutoKeyword: [],
				courtAutoKeyword: [],
				courtIdAutoKeyword: [],
				autoCompleteState: false,
			})
		} else {
			this.setState({
				autoCompleteState: true,
			})
		}
		if (event.key === 'Enter') {
			this.setState({
				autoCompleteState: false,
			})
		}
		if (event.key !== 'Enter') {
			// console.log('noenter');
			const { keyword } = this.state;
			// console.log(keyword);
			if (keyword === '') {
				this.setState({
					regionAutoKeyword: [],
					courtAutoKeyword: [],
					courtIdAutoKeyword: [],
					autoCompleteState: false,
				})
			}
			getAutoComplete(keyword).then((request) => {
				// console.log(request);
				let regionData = request.data.region;
				let courtData = request.data.court;
				let courtIdData = request.data.court_id;
				if (regionData[0] === '') {
					regionData = [];
				}
				if (courtData[0] === '') {
					courtData = [];
				}
				if (courtIdData[0] === '') {
					courtIdData = [];
				}

				if (keyword === '') {
					this.setState({
						regionAutoKeyword: regionData,
						courtAutoKeyword: courtData,
						courtIdAutoKeyword: courtIdData,
						autoCompleteState: false,
					})
				} else {
					this.setState({
						regionAutoKeyword: regionData,
						courtAutoKeyword: courtData,
						courtIdAutoKeyword: courtIdData,
						autoCompleteState: true,
					})
				}
			});
			return;
		}
		object.context.router.history.push('/');
		if (!this.state.enter) {
			// console.log('Enter!');
			this.setState({
				autoCompleteState: false,
			});
			// 주소-좌표 변환 객체를 생성합니다
			const { keyword } = this.state;
			// const geocoder = new mapObject.services.Geocoder();
			// console.log('keyowrd', keyword);
            getSearch(keyword).then((request) => {
                // console.log('getSearch : ', request);
                object.props.reLoad(request.data);
            })
			// 주소로 좌표를 검색합니다
			// geocoder.addressSearch(keyword, function (result, status) {
			// 	console.log('reuslt', result);
			// 	console.log('status', status);
			// 	if (status === mapObject.services.Status.OK) {
            //         console.log('확인')
			// 		const args = {
			// 			lat: Number(result[0].y),
			// 			lng: Number(result[0].x),
			// 		};
			// 		object.props.reLoad(args);
			// 	} else {
			// 		console.log('확인');
			// 		getSearchMapXY(keyword).then((request) => {
			// 			if (request.data.x || request.data.y) {
			// 				const args = {
			// 					lat: Number(request.data.x),
			// 					lng: Number(request.data.y),
			// 				};
			// 				object.props.reLoad(args);
			// 			} else {
			// 				alert('검색 결과가 없습니다.');
			// 			}
			// 		});
			// 	}
			// });
		}
	}
	render() {
		return (
			<nav className="navbar">
				<div className="row clearfix">
					<div className="col-8">
						<div className="logo">
							<li>
								<Link to="/">
									<img src={logo} alt="logo" />
								</Link>
							</li>
						</div>
						<div className="search-bx">
							<div className="search-group">
								<button type="submit" className="btn-search">
									<i className="mdi mdi-magnify" />
								</button>
								<input
									onFocus={this.handleFocusIn}
									onBlur={this.handleFocusOut}
									autoComplete="off"
									type="text"
									className="form-control"
									name="inputSearch"
									onChange={this.handleChange}
									onKeyUp={this.keyUp}
									value={this.state.keyword}
									placeholder="법원, 사건번호로 검색해 보세요."
								/>
							</div>
							{this.state.autoCompleteState &&
							<AutoComplete
								handleFocusOut={this.handleFocusOut}
								regionList={this.state.regionAutoKeyword}
								courtList={this.state.courtAutoKeyword}
								courtIdList={this.state.courtIdAutoKeyword}
								handleClick={this.handleClick}
								autoCompleteState={this.state.autoCompleteState}
							/>}
						</div>
					</div>
					<div className="col-4">
						<div className="navbar-nav">
							<ul>
								<li>
									<Link to="/">
										홈
									</Link>
								</li>
								<li>
									<Link to="/login">
										로그인
									</Link>
								</li>
								<li>
									<Link to="/resister">
										회원가입
									</Link>
								</li>
								<li>
									<a href="http://naver.com" onClick={this.testHandle}>
										뒤로가기
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		);
	}
}

function reload(args) {
	console.log('Header reload : ', args);
	return {
		// type: '',
		// lat: args.lat ? args.lat : 0,
		// lng: args.lng ? args.lng : 0,
        type: '',
        records: args && args.records ? args.records : [],
        foundRecord: args && args.foundRecord ? args.foundRecord : 0,
        currentPage: args && args.currentPage ? args.currentPage : 1,
        hq: args && args.hq ? args.hq : 1,
		query: args && args.query ? args.query : 1,
        searchFlag: args && args.searchFlag ? args.searchFlag : '',
	};
}

const headerDispatchToProps = dispatch => ({
	reLoad: args => dispatch(reload(args)),
});

Header = connect(undefined, headerDispatchToProps)(Header);

export default Header;
