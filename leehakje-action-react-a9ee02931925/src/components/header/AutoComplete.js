import React, { Component } from 'react';
import * as PropTypes from 'prop-types';


class AutoComplete extends Component {
    static defultProps = {
        list : [],
    };
    render() {
        const { regionList, courtList, courtIdList } = this.props;
        // const { autoCompleteState } = this.props;
        const { handleClick } = this.props;
        const regionItemList = regionList.map(
            item => (
                <li onClick={() => handleClick(item)}>
                    <span className="search-badge region">지역</span>
                    <div className="result-txt">{item}</div>
                </li>
                ));
        const courtItemList = courtList.map(
            item => (
                <li onClick={() => handleClick(item)}>
                    <span className="search-badge court">법원</span>
                    <div className="result-txt">{item}</div>
                </li>
                ));
        const courtIdItemList = courtIdList.map(
            item => (
                <li onClick={() => handleClick(item)}>
                    <span className="search-badge number">사건번호</span>
                    <div className="result-txt number-txt">{item}</div>
                </li>
                ));
        return (
            <div className="search-result clearfix" >
                <div className="result-type">
                    <ul>
                    <li>
                        <p className="radio">
                        <input type="radio" checked="checked" name="r01" id="r011" />
                        <label htmlFor="r011">전체</label>
                        </p>
                    </li>
                    <li>
                        <p className="radio">
                        <input type="radio" name="r01" id="r012" />
                        <label htmlFor="r012">법원</label>
                        </p>
                    </li>
                    <li>
                        <p className="radio">
                        <input type="radio" name="r01" id="r013" />
                        <label htmlFor="r013">지역</label>
                        </p>
                    </li>
                    <li>
                        <p className="radio">
                        <input type="radio" name="r01" id="r014" />
                        <label htmlFor="r014">사건번호</label>
                        </p>
                    </li>
                    </ul>
                </div>
                <div className="result-list-box">
                    <div className="result-count"><span>총 {courtList.length + regionList.length + courtIdList.length}개</span>의 검색결과</div>
                    <ul>
                        {regionItemList}
                        {courtItemList}
                        {courtIdItemList}
                    </ul>
                </div>
            </div>
        );
    }
}

AutoComplete.propTypes = {
        // autoCompleteState : PropTypes.object.isRequired,
        regionList : PropTypes.array.isRequired,
        courtList : PropTypes.array.isRequired,
        courtIdList: PropTypes.array.isRequired,
        handleClick: PropTypes.func.isRequired,
};

export default AutoComplete;
