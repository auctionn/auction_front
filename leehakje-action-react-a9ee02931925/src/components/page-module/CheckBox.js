import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CheckBox extends Component {
	static defaultProps = {
		idx: 0,
		name: '',
	}
	render() {
		return (
			<fragment>
				<input className="all-check-type" type="checkbox" id={this.props.id} name={this.props.name} value={this.props.idx} onChange={this.props.onChange} checked={this.props.checked} />
				<label htmlFor={this.props.id}>{this.props.label}</label>
			</fragment>
		);
	}
}
CheckBox.propTypes = {
	id: PropTypes.string.isRequired,
	idx: PropTypes.number,
	onChange: PropTypes.func.isRequired,
	checked: PropTypes.bool.isRequired,
	label: PropTypes.string.isRequired,
	name: PropTypes.string,
};
export default CheckBox;
