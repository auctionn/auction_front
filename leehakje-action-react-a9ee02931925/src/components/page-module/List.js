/* eslint-disable no-undef,prefer-destructuring,space-before-function-paren,function-paren-newline,prefer-arrow-callback,import/first */
/* eslint-disable react/no-unescaped-entities */
import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Pagination from 'react-js-pagination';
import ListItem from '../page-module/ListItem';
import $ from 'jquery';
// import { connect } from 'react-redux';

let object;
let gFocusId = '';
class List extends Component {
	constructor(props) {
		super(props);
		object = this;
		this.state = ({
			value: 'non-select',
		});
		this.onPaged = this.onPaged.bind(this);
		this.selectChange = this.selectChange.bind(this);
    }
	componentDidMount() {
		$(document).ready(() => {
			// $('select.pretty').prettyDropdown();
			// $('select.rd-select').on('change', this.selectChange.bind(this));
			$(document).on('mouseover', 'div.post', function() {
				let focusID = '0';
				if ($(this).hasClass('post')) {
					focusID = $(this).attr('record-id');
				} else {
					let tempObj = $(this);
					while (!tempObj.hasClass('post')) {
						tempObj = $(this).parent();
					}
					focusID = tempObj.attr('record-id');
				}
				if (focusID !== gFocusId) {
					$('div.overlay_info .list-marker, div.overlay_info .marker-locate').removeClass('current-list-marker');
					$('div').removeClass('current-focus');
					$(`div.overlay_info[record-id='${focusID}'] .list-marker, div.overlay_info[record-id='${focusID}'] .list-marker .marker-locate`).addClass('current-list-marker');
					$(`div.overlay_info[record-id='${focusID}']`).parent().addClass('current-focus');
					gFocusId = focusID;
				}
			});
			$(document).on('mouseout', 'div.post', function() {
				let focusID = '0';
				if ($(this).hasClass('post')) {
					focusID = $(this).attr('record-id');
				} else {
					let tempObj = $(this);
					while (!tempObj.hasClass('post')) {
						tempObj = $(this).parent();
					}
					focusID = tempObj.attr('record-id');
				}
				if (focusID === gFocusId) {
					$('div.overlay_info .list-marker, div.overlay_info .marker-locate').removeClass('current-list-marker');
					$('div').removeClass('current-focus');
					gFocusId = '';
				}
			})
		});
	}
    componentWillUpdate(nextProps) {
        // console.log('List Will Update : ', nextProps);
        if (nextProps.searchFlag === 'true') {
            // console.log('Query IN', nextProps);
            nextProps.onChange(nextProps);
        }
    }
	selectChange(event) {
		console.log(object);
		this.setState({
			value: event.target.value,
		});
		const args = [];
		args.orderby = event.target.value;
		args.query = this.props.query;
		console.log('in selectChange', args, ',  event:', event.target.value);
		this.props.onChange(args);
	}
	onPaged(currentPage) {
		console.log('page', this.state.value);
		const args = [];
		args.orderby = this.state.value;
		args.currentPage = currentPage;
		args.query = this.props.query;
		this.props.onChange(args);
		console.log('in onPaced', args.orderby, ', event:', this.state.value);
	}
	render() {
        // console.log('List');
		let active = 'row text-right';
		if (this.props.foundRecord === 0) {
			active += ' hide';
		}
		return (
			<div>
				<div className={active}>
					<div className="dropdown">
						<select className="select-dropdown" onChange={this.selectChange}>
							<option value="non-select">선택안함</option>
							<option value="case-number-asc">사건번호↑</option>
							<option value="case-number-desc">사건번호↓</option>
							<option value="disposal-date-asc">매각기일↑</option>
							<option value="disposal-date-desc">매각기일↓</option>
							<option value="appraise-asc">감정가↑</option>
							<option value="appraise-desc">감정가↓</option>
							<option value="min-asc">최저가↑</option>
							<option value="min-desc">최저가↓</option>
							<option value="miscarriage-asc">유찰횟수↑</option>
							<option value="miscarriage-desc">유찰횟수↓</option>
						</select>
						<h1 style={{ textAlign: 'left', fontSize: '17px' }}>검색 결과 물건 {this.props.foundRecord}개</h1>
					</div>
				</div>
				{this.props.foundRecord === 0 ?
					<div className="no-result list-section text-center">
						<h2>
							<span>'검색한 키워드'</span>로 검색한 결과가 없습니다.
						</h2>
						<a className="back" href="/">리스트로 되돌아가기</a>
					</div>
					:
					<div>
						<div className="row gutter-sm clearfix">
							{this.props.records.map((row, index) => {
								return (
								<ListItem
									key={row.ID}
									data={row}
									reactkey={index}
									ref={`item${index}`}
									obj={this}
									goDetail={this.props.goDetail}
								/>)
							})}
						</div>
						<div className="page">
							<Pagination
								activePage={this.props.currentPage}
								itemsCountPerPage={this.props.recordPerPage}
								totalItemsCount={this.props.foundRecord}
								pageRangeDisplayed={5}
								onChange={this.onPaged}
								linkClass="page-link"
								innerClass="pagination text-center"
								itemClass="page-item"
								activeClass="current"
								linkClassNext="next"
								linkClassPrev="pre"
								linkClassFirst="pre"
								linkClassLast="next"
								hideDisabled="true"
							/>
						</div>
					</div>
				}
			</div>
		);
	}
}

List.propTypes = {
	records: PropTypes.array.isRequired,
	currentPage: PropTypes.number.isRequired,
	recordPerPage: PropTypes.number.isRequired,
	foundRecord: PropTypes.number.isRequired,
	onChange: PropTypes.func.isRequired,
	query : PropTypes.string.isRequired,
	goDetail: PropTypes.func.isRequired,
	// status: PropTypes.bool.isRequired,
};

export default List;
