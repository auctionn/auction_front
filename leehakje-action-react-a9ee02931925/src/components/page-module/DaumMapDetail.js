import React, { Component } from 'react';
import PropTypes from 'prop-types';
import getBuildingData from '../../utils/utilSearchRcords';
import getCenter from '../../utils/utilGetCenter';
// import getActualAvg from '../../utils/utilActualAvg';

// let mapObject = null;
let map = null;
class DaumMapDetail extends Component {
	constructor(props) {
			super(props);
			this.state = {
				disAlert: 'show',
				buildingRecords: [],
				centerLng: '',
				centerLat: '',
			};
		this.daumMap = window.daum.maps;
	}
	shouldComponentUpdate(nextProps, nextState) {
		// console.log('shouldComponentUpdate: ', JSON.stringify(nextProps), ' ', JSON.stringify(this.props));
		return ((JSON.stringify(nextProps) !== JSON.stringify(this.props)) || (JSON.stringify(nextState) !== JSON.stringify(this.state)));
	}
	componentDidMount() {
		// console.log(this.state.centerLat, this.state.centerLng);
		const el = document.getElementById('map');
		const level = 5;
		const zoomControl = new this.daumMap.ZoomControl();
		this.obj = this;
		// mapObject = this;
		this.markers = [];
		this.contents = [];
		this.mapCustomOverlay = new this.daumMap.CustomOverlay();
		getCenter(this.props.postid).then((request) => {
			// console.log(request);
			if ('data' in request) {
				this.setState({
					centerLat: request.data.records[0].lat,
					centerLng: request.data.records[0].lng,
				});
				// console.log(this.state.centerLng);
				map = new this.daumMap.Map(el, {
					center: new this.daumMap.LatLng(this.state.centerLat, this.state.centerLng),
					level: level,
				});
				const clusterer = new this.daumMap.MarkerClusterer({
					map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
					averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
					minLevel: 6, // 클러스터 할 최소 지도 레벨
					markers: this.markers,
					gridSize: 60,
					disableClickZoom: true, // 클러스터 클릭 시 지도 확대 되지 않는다
					styles: [{
						width: '43px',
						height: '42px',
						background: '#1e47b0 no-repeat',
						borderRadius: '100%',
						color: '#fff',
						textAlign: 'center',
						lineHeight: '43px',
						fontSize: '15px',
						opacity: 0.87,
					}],
				});
				this.map = map;
				this.clusterer = clusterer;
				this.map.addControl(zoomControl, this.daumMap.ControlPosition.RIGHT);
				this.onDetailMapDataChange(this.getLatLng());
				this.daumMap.event.addListener(map, 'dragend', () => {
					this.removeMarker();
					this.mapCustomOverlay.setMap(null);
					this.args = this.getLatLng();
					this.onDetailMapDataChange(this.args);
				});
				this.daumMap.event.addListener(map, 'zoom_changed', () => {
					this.removeMarker();
					this.args = this.getLatLng();
					this.onDetailMapDataChange(this.args);
				});
			}
		});
	}
	addEventHandle(target, type, callback) {
		// target node에 이벤트 핸들러를 등록하는 함수힙니다
		if (target.addEventListener) { // Internet Explorer 11
			target.addEventListener(type, callback);
		} else { // below version 11
			target.attachEvent(`on${type}`, callback);
		}
	}
	removeEventHandle(target, type, callback) {
		// target node에 등록된 이벤트 핸들러를 제거하는 함수힙니다
		if (target.removeEventListener) {
			target.removeEventListener(type, callback);
		} else {
			target.detachEvent(`on${type}`, callback);
		}
	}
	removeMarker() {
		if (this.markers) {
			if (this.markers.length > 0) {
				for (let i = 0; i < this.markers.length; i += 1) {
					this.markers[i].setMap(null);
					this.removeEventHandle(this.contents[i], 'mousemove', this.onMouseDown);
				}
				this.markers = [];
				this.contents = [];
			}
		}
	}
	getLatLng() {
		const bounds = this.map.getBounds();
		// 영역의 남서쪽 좌표를 얻어옵니다
		const swLatLng = bounds.getSouthWest();
		// 영역의 북동쪽 좌표를 얻어옵니다
		const neLatLng = bounds.getNorthEast();
		const message = `지도의 남서쪽 좌표는 ${swLatLng.getLat()}, ${swLatLng.getLng()} 이고 <br>
		북동쪽 좌표는 ${neLatLng.getLat()}, ${neLatLng.getLng()} 입니다`;
		const args = {
			sw_lat: swLatLng.getLat(),
			sw_lng: swLatLng.getLng(),
			ne_lat: neLatLng.getLat(),
			ne_lng: neLatLng.getLng(),
			message: message,
		};
		return args;
	}
	onDetailMapDataChange(params) {
		getBuildingData(params).then((request) => {
			if ('data' in request) {
				this.setState({
					buildingRecords: request.data.records,
				});
			}
			// console.log('after', this.state.buildingRecords);
		});
	}
	addMarker(row, count) {
		const content = document.createElement('div');
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		// console.log('in addmarker', this.convertToPrice(row.sixAvg)); // current marker와는 다른 row 정보가 들어오고 있다.
		// const minPrice = row.Appraisal_min_price ? this.convertToPrice(row.price[0]) : '정보 없음';
		let minPrice = '정보 없음'
		if (Number.isInteger(row.sixAvg)) {
			minPrice = '';
            // const Reminder = row.sixAvg % 100;
			// const HundredPrice = parseInt(row.sixAvg / 100, 10) % 10;
			const ThousandPrice = parseInt(row.sixAvg / 1000, 10) % 10;
			const MillionPrice = parseInt(row.sixAvg / 10000, 10) % 10;
			minPrice += MillionPrice !== 0 ? `${String(MillionPrice)}억 ` : '';
			minPrice += ThousandPrice !== 0 ? `${String(ThousandPrice)}천 ` : '';
			// minPrice += HundredPrice !== 0 ? `${String(HundredPrice)}백 ` : '';
			// minPrice += '만원';
		}
		// const minPrice = Number.isInteger(row.sixAvg) ? row.Appraisal_min_price : '정보 없음';
		// const args = {
		// 	address: row.newPlatPlc,
        //     buildnm: row.buildnm,
		// };
		// let minPrice = '정보 없음';
        // getActualAvg(args).then((request) => {
        // 	// console.log(request);
		// 	minPrice = Number.isInteger(request.records.oneavg) ? this.convertToPrice(request.records.oneavg) : 0;
        // });
        // console.log(row);
		const buildingHide = count === 1 ? 'hide' : '';
		content.innerHTML = `<div class='post-marker'>
							<div class="kind">
								<span>${row.etcPurps}</span>
							</div>
							<div class='average-price'>
								${minPrice}
							</div>
							<div class=${buildingHide}>
								<div class='counting'}>같은 위치 물건
									<span>${count - 1}</span>건
								</div>
							</div>
						</div>`;
		// 커스텀 오버레이가 표시될 위치입니다
		const po = new this.daumMap.LatLng(row.lat, row.lng);
		// 커스텀 오버레이를 생성합니다
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: po,
			content: content,
			// xAnchor: 0.5, // 커스텀 오버레이의 x축 위치입니다. 1에 가까울수록 왼쪽에 위치합니다. 기본값은 0.5 입니다
			// yAnchor: 1.1 // 커스텀 오버레이의 y축 위치입니다. 1에 가까울수록 위쪽에 위치합니다. 기본값은 0.5 입니다
		});
		// 커스텀 오버레이를 지도에 표시합니다
		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		content.addEventListener('click', () => {
			this.map.panTo(po);
			this.props.buildingPushData(row);
		});
	}
	addCurrentMarker(row) {
		const content = document.createElement('div');
		const minPrice = row.Appraisal_min_price ? this.convertToPrice(row.price[0]) : '정보 없음';
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		// console.log(row);
		content.innerHTML = `<div class='current-marker'>
							<div class="kind">
								<span>현재물건</span>
							</div>
							<div class='average-price'>
								${minPrice}
							</div>
						</div>`;
		// 커스텀 오버레이가 표시될 위치입니다
		const po = new this.daumMap.LatLng(row.lat, row.lng);
		// 커스텀 오버레이를 생성합니다
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: po,
			content: content,
			// xAnchor: 0.5, // 커스텀 오버레이의 x축 위치입니다. 1에 가까울수록 왼쪽에 위치합니다. 기본값은 0.5 입니다
			// yAnchor: 1.1 // 커스텀 오버레이의 y축 위치입니다. 1에 가까울수록 위쪽에 위치합니다. 기본값은 0.5 입니다
		});
		// 커스텀 오버레이를 지도에 표시합니다
		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		this.addEventHandle(content, 'click', this.onMouseDown);
	}
	convertToPrice(price) {
		let minPriceStr = '';
		const intPriceHundred = Math.floor(price / 1000000) % 10;
		const intPriceThousand = Math.floor(price / 10000000) % 10;
		const intPriceHundredMillion = Math.floor(price / 100000000);
		const chun = intPriceThousand === 0 ? '' : `${intPriceThousand}천`;
		const baek = intPriceHundred === 0 ? '' : `${intPriceHundred}백`;
		// console.log(price);
		// console.log(`${intPriceHundredMillion}억${intPriceThousand}천${intPriceHundred}백`);
		minPriceStr = price >= 100000000 ? `${intPriceHundredMillion}억 ${chun}` : `${chun} ${baek}`;
		return minPriceStr;
	}
	disAlertHide = () => {
		this.setState({
			disAlert: 'hide',
		});
	};
	render() {
		if (this.daumMap) {
			this.removeMarker(); // 수정한 부분
			const count = [];
			const xLoc = [];
			const yLoc = [];
			// console.log('building records', this.state.buildingRecords);
			// buildingList: 중복 제거한 building 리스트
			const buildingList = [];
			this.state.buildingRecords.forEach((row) => {
				// console.log(row);
				// console.info('row info', row);
				const flag = xLoc.indexOf(row.lng);
				// flag가 -1이면 겹치는 위치가 존재하지 않으므로 새로운 데이터를 push하면 된다.
				// marker도 새로 찍으면 된다.
				if (flag === -1) {
					// this.addMarker(row);
					buildingList.push(row);
					xLoc.push(row.lng);
					yLoc.push(row.lat);
					count.push(1);
				} else if (flag !== -1 && yLoc.indexOf(row.lat, flag)) {
					count[flag] += 1;
				}
				this.clusterer.addMarkers(this.markers);
			});
			buildingList.forEach((row) => {
				const index = xLoc.indexOf(row.lng);
				this.addMarker(row, count[index]);
			});
			// console.log('count', count);
			// console.log('court data', this.props.courtData);
			this.props.courtData.forEach((row) => {
				// console.info(row);
				this.addCurrentMarker(row);
			});
		}
		return (
			<div className="col-4 map-section">
				<div className="App map" id="map" />
				<div className={`alert alert-dismissible fade ${this.state.disAlert}`}>
				현재 물건 주변 아파트 실거래가 정보를 알아보실 수 있습니다. <button type="button" className="close" aria-label="Close" onClick={this.disAlertHide}>
				<span><i className="mdi mdi-close" /></span>
				</button>
				</div>
			</div>
		);
	}
}
DaumMapDetail.propTypes = {
	postid: PropTypes.number.isRequired,
	courtData: PropTypes.array.isRequired,
	// buildingRecords: PropTypes.array.isRequired,
	buildingPushData: PropTypes.func.isRequired,
	// // foundRecord: PropTypes.number.isRequired,
	// // currentPage: PropTypes.number.isRequired,
	// onChange: PropTypes.func.isRequired,
	// recordPerPage: PropTypes.number.isRequired,
};

export default DaumMapDetail;
