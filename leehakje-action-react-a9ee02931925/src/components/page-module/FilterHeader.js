import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DayPicker from 'react-day-picker';
import CheckBox from './CheckBox';
import '../../../node_modules/react-day-picker/lib/style.css';


class FilterHeader extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectStartDay: '',
			selectEndDay: '',
			kindState: false,
			ingState: false,
			dateState: false,
			appraiseState: false,
			minState: false,
			addfilterState: false,
			addMoreState: false,
			args: {
				hq: '',
				msq: '',
				mis: '',
				date: '',
				app: '',
				min: '',
				add: '',
				plot: '', // 대지면적
				area: '', // 건물면적
				num: '', // 지번범위
			},
			house: [
				{
					id: 'apt',
					label: '아파트',
					checked: true,
				},
				{
					id: 'housing',
					label: '단독주택',
					checked: true,
				},
				{
					id: 'neighborhoodhouse',
					label: '연립주택',
					checked: true,
				},
				{
					id: 'room',
					label: '다가구',
					checked: true,
				},
				{
					id: 'villa',
					label: '빌라',
					checked: true,
				},
			],
			commerce: [
				{
					id: 'mall',
					label: '상가',
					checked: true,
				},
				{
					id: 'facility',
					label: '근린시설',
					checked: true,
				},
				{
					id: 'officetel',
					label: '오피스텔',
					checked: true,
				},
				{
					id: 'office',
					label: '사무실',
					checked: true,
				},
				{
					id: 'storage',
					label: '창고',
					checked: true,
				},
				{
					id: 'factory',
					label: '공장',
					checked: true,
				},
				{
					id: 'apttypefactory',
					label: '아파트형 공장',
					checked: true,
				},
				{
					id: 'accommodation',
					label: '숙박시설',
					checked: true,
				},
				{
					id: 'etcaccommodation',
					label: '숙박(콘도 등)',
					checked: true,
				},
				{
					id: 'education',
					label: '교육시설',
					checked: true,
				},
				{
					id: 'religion',
					label: '종교시설',
					checked: true,
				},
				{
					id: 'farm',
					label: '농가관련시설',
					checked: true,
				},
				{
					id: 'medical',
					label: '의료시설',
					checked: true,
				},
				{
					id: 'dynamite',
					label: '주유소(위험물)',
					checked: true,
				},
				{
					id: 'bath',
					label: '목욕탕',
					checked: true,
				},
				{
					id: 'infirm',
					label: '노유자시설',
					checked: true,
				},
				{
					id: 'waste',
					label: '분뇨쓰레기처리',
					checked: true,
				},
				{
					id: 'car',
					label: '자동차관련시설',
					checked: true,
				},
				{
					id: 'funeral',
					label: '장례관련시설',
					checked: true,
				},
				{
					id: 'culture',
					label: '문화및집회시설',
					checked: true,
				},
			],
			exceptra: [
				{
					id: 'mining',
					label: '광업권',
					checked: true,
				},
				{
					id: 'fishery',
					label: '어업권',
					checked: true,
				},
				{
					id: 'salt',
					label: '염전',
					checked: true,
				},
				{
					id: 'fishfarm',
					label: '양어장',
					checked: true,
				},
				{
					id: 'etc',
					label: '기타',
					checked: true,
				},
			],
			land: [
				{
					id: 'site',
					label: '대지',
					checked: true,
				},
				{
					id: 'farmland',
					label: '농지',
					checked: true,
				},
				{
					id: 'forest',
					label: '임야',
					checked: true,
				},
				{
					id: 'hybrid',
					label: '잡종지',
					checked: true,
				},
				{
					id: 'orchard',
					label: '과수원',
					checked: true,
				},
				{
					id: 'road',
					label: '도로',
					checked: true,
				},
				{
					id: 'cemetery',
					label: '묘지',
					checked: true,
				},
				{
					id: 'ranch',
					label: '목장용지',
					checked: true,
				},
				{
					id: 'plant',
					label: '공장용지',
					checked: true,
				},
				{
					id: 'school',
					label: '학교용지',
					checked: true,
				},
				{
					id: 'depot',
					label: '창고용지',
					checked: true,
				},
				{
					id: 'physical',
					label: '체육용지',
					checked: true,
				},
				{
					id: 'religionpot',
					label: '종교용지',
					checked: true,
				},
				{
					id: 'etcpot',
					label: '기타용지',
					checked: true,
				},
				{
					id: 'ditch',
					label: '구거',
					checked: true,
				},
				{
					id: 'river',
					label: '하천',
					checked: true,
				},
				{
					id: 'oilpaper',
					label: '유지',
					checked: true,
				},
				{
					id: 'dike',
					label: '제방',
					checked: true,
				},
				{
					id: 'parking',
					label: '주차장',
					checked: true,
				},
			],
			traffic: [
				{
					id: 'sedan',
					label: '승용차',
					checked: true,
				},
				{
					id: 'bus',
					label: '버스',
					checked: true,
				},
				{
					id: 'truck',
					label: '화물차',
					checked: true,
				},
				{
					id: 'heavyequipment',
					label: '중장비',
					checked: true,
				},
				{
					id: 'ship',
					label: '선박',
					checked: true,
				},
			],
			ingFilter: [
				{
					id: 'progress',
					label: '진행물건',
					checked: true,
				},
				{
					id: 'new',
					label: '신건',
					checked: true,
				},
				{
					id: 'miscarriage',
					label: '유찰',
					checked: true,
				},
				{
					id: 'disposal',
					label: '매각',
					checked: true,
				},
				{
					id: 'permission',
					label: '매각허가',
					checked: true,
				},
				{
					id: 'paybalance',
					label: '잔금납부물건',
					checked: true,
				},
				{
					id: 'distribution',
					label: '배당기일종결',
					checked: true,
				},
				{
					id: 'etcprogress',
					label: '진행 외 물건',
					checked: true,
				},
				{
					id: 'nonprogress',
					label: '미진행',
					checked: true,
				},
				{
					id: 'change',
					label: '변경/연기',
					checked: true,
				},
				{
					id: 'nonpermission',
					label: '불허가/허가취소',
					checked: true,
				},
				{
					id: 'stop',
					label: '정지',
					checked: true,
				},
				{
					id: 'closeobject',
					label: '종국물건',
					checked: true,
				},
				{
					id: 'dismissal',
					label: '각하',
					checked: true,
				},
				{
					id: 'reject',
					label: '기각',
					checked: true,
				},
				{
					id: 'etcstatue',
					label: '기타',
					checked: true,
				},
				{
					id: 'transfer',
					label: '이송',
					checked: true,
				},
				{
					id: 'cancel',
					label: '취소',
					checked: true,
				},
				{
					id: 'drop',
					label: '취하',
					checked: true,
				},
				{
					id: 'terminate',
					label: '모사건 종결',
					checked: true,
				},
			],
			special: [
				{
					id: 'todaynew',
					label: '오늘 공고된 신건',
					checked: true,
				},
				{
					id: 'resale',
					label: '재매각 물건 검색',
					checked: true,
				},
				{
					id: 'halfsale',
					label: '반값 경매 물건',
					checked: true,
				},
				{
					id: 'afteroneyear',
					label: '감정일에서 1년이 지난 물건',
					checked: true,
				},
				{
					id: 'violation',
					label: '위반 건축물',
					checked: true,
				},
				{
					id: 'setting',
					label: '전세권/임차권 설정',
					checked: true,
				},
			],
			specialadd: [
				{
					id: 'specialadd01',
					label: '선순위 전세권/임차권 설정',
					checked: true,
				},
				{
					id: 'specialadd02',
					label: '임차권 등기',
					checked: true,
				},
				{
					id: 'specialadd03',
					label: '전세권설정/임차권등기',
					checked: true,
				},
				{
					id: 'specialadd04',
					label: '말소기준등기보다 앞선 임차권',
					checked: true,
				},
				{
					id: 'specialadd05',
					label: '대항력 있는 임차인',
					checked: true,
				},
				{
					id: 'specialadd06',
					label: '선순위 가등기',
					checked: true,
				},
				{
					id: 'specialadd07',
					label: '선순위 가처분',
					checked: true,
				},
				{
					id: 'specialadd08',
					label: '예고등기',
					checked: true,
				},
				{
					id: 'specialadd09',
					label: '대지권미등기',
					checked: true,
				},
				{
					id: 'specialadd10',
					label: '토지별도등기 있는 물건',
					checked: true,
				},
				{
					id: 'specialadd11',
					label: '토지별도등기인수조건',
					checked: true,
				},
				{
					id: 'specialadd12',
					label: '건물만 입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd13',
					label: '토지만 입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd14',
					label: '지분입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd15',
					label: '유치권 신고된 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd16',
					label: '법정지상권 여지 있는 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd17',
					label: '분묘기지권 성립여지',
					checked: true,
				},
				{
					id: 'specialadd18',
					label: '유치권에 의한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd19',
					label: '공유물분할을 위한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd20',
					label: '청산을 위한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd21',
					label: '기타 형식적경매',
					checked: true,
				},
			],
			procedure: [
				{
					id: 'all',
					label: '전체보기',
					checked: true,
				},
				{
					id: 'random',
					label: '임의경매',
					checked: true,
				},
				{
					id: 'force',
					label: '강제경매',
					checked: true,
				},
			],
			startDate: this.formatDate(new Date()),
			endDate: this.formatDate(new Date()),
			numselect: '',
			kindAllChecked: true,
			houseAllChecked: true,
			commerceAllChecked: true,
			landAllChecked: true,
			trafficAllChecked: true,
			exceptraAllChecked: true,
			ingFilterAllChecked: true,
			specialAllChecked: true,
			procedureAllChecked: true,
		};
		this.args = {
			hq: '',
			msq: '',
			mis: '',
			date: '',
			app: '',
			min: '',
			add: '',
			plot: '', // 대지면적
			area: '', // 건물면적
			num: '', // 지번범위
			misstart: '',
			misend: '',
			appstart: '',
			append: '',
			minstart: '',
			minend: '',
			plotstart: '',
			plotend: '',
			areastart: '',
			areaend: '',
			numstart: '',
			numend: '',
		}
		this.focus = {
			kindCount: 0,
			ingCount: 0,
			addCount: 0,
			kindActive: 'hide',
			ingActive: 'hide',
			dateActive: 'hide',
			appActive: 'hide',
			misnumActive: '',
			minActive: 'hide',
			addActive: 'hide',
			addMoreActive: 'hide',
			kindColorActive: '',
			ingColorActive: '',
			dateColorActive: '',
			appColorActive: '',
			minColorActive: '',
			addColorActive: '',
		}
		this.str = '';
		this.handleCheck = this.handleCheck.bind(this);
		this.handleStartDate = this.handleStartDate.bind(this);
		this.handleEndDate = this.handleEndDate.bind(this);
		this.selectChange = this.selectChange.bind(this);
		this.selectAllChange = this.selectAllChange.bind(this);
		this.changeInput = this.changeInput.bind(this);
		this.changeDayPick = this.changeDayPick.bind(this);
		this.filterInit = this.filterInit.bind(this);
		this.filterEvent = this.filterEvent.bind(this);
		this.filterSubmit = this.filterSubmit.bind(this);
	}
	filterInit(event) {
		if (event.target.id === 'kind') {
			this.setState({
				kindAllChecked: true,
			});
			let targetName = 0;
			let index = 0;
			const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
			for (targetName = 0; targetName < nameArray.length; targetName += 1) {
				for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
					if (this.state[nameArray[targetName]][index].checked === false) {
						this.changeCheckState(nameArray[targetName], index);
					}
				}
			}
			this.focus.kindActive = 'hide';
			this.focus.kindColorActive = '';
		} else if (event.target.id === 'Ing') {
			this.setState({
				ingFilterAllChecked: true,
			});
			let index = 0;
			for (index = 0; index < this.state.ingFilter.length; index += 1) {
				if (this.state.ingFilter[index].checked === false) {
					this.changeCheckState('ingFilter', index);
				}
			}
			if (this.state.ingFilter[2].checked === true) {
				this.focus.misnumActive = '';
			} else {
				this.focus.misnumActive = 'hide';
			}
			this.args.misstart = '-1';
			this.args.misend = '-1';
			this.args.mis = '';
			this.focus.ingActive = 'hide';
			this.focus.ingColorActive = '';
		} else if (event.target.id === 'Date') {
			this.setState({
				startDate: '-1',
				endDate: '-1',
				selectStartDay: '',
				selectEndDay: '',
			});
			this.args.date = '';
			this.focus.dateActive = 'hide';
			this.focus.dateColorActive = '';
		} else if (event.target.id === 'App') {
			this.args.appstart = '-1';
			this.args.append = '-1';
			this.args.app = '';
			this.focus.appActive = 'hide';
			this.focus.appColorActive = '';
		} else if (event.target.id === 'Min') {
			this.args.minstart = '-1';
			this.args.minend = '-1';
			this.args.min = '';
			this.focus.minActive = 'hide';
			this.focus.minColorActive = '';
		} else if (event.target.id === 'Add') {
			let index = 0;
			for (index = 0; index < this.state.special.length; index += 1) {
				if (this.state.special[index].checked === false) {
					this.changeCheckState('special', index);
				}
			}
			for (index = 0; index < this.state.procedure.length; index += 1) {
				if (this.state.procedure[index].checked === false) {
					this.changeCheckState('procedure', index);
				}
			}
			this.args.plotstart = '';
			this.args.plotend = '';
			this.args.areastart = '';
			this.args.areaend = '';
			this.args.numstart = '';
			this.args.numend = '';
			this.setState({
				numselect: '',
			});
			this.args.plot = '';
			this.args.area = '';
			this.args.num = '';
			this.focus.addActive = 'hide';
			this.focus.addColorActive = '';
		}
	}
	changeAllStatus = (targetIndex, flag) => {
		// 전체선택 시 아기 체크박스 변경되는 액션
		let index = 0;
		const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra', 'ingFilter', 'special', 'specialadd', 'procedure'];
		if (flag) {
			for (index = 0; index < this.state[nameArray[targetIndex]].length; index += 1) {
				if (this.state[nameArray[targetIndex]][index].checked === false) {
					this.changeCheckState(nameArray[targetIndex], index);
				}
			}
		} else {
			for (index = 0; index < this.state[nameArray[targetIndex]].length; index += 1) {
				if (this.state[nameArray[targetIndex]][index].checked === true) {
					this.changeCheckState(nameArray[targetIndex], index);
				}
			}
		}
	}
	selectAllChange(event) {
		if (event.target.id === 'kindAllCheck') { // 대왕 전체선택
			// click 할 때 selectAll의 check state를 변경한다.
			const flag = !(this.state.kindAllChecked);
			this.setState({
				kindAllChecked: flag,
			});
			let targetName = 0;
			const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
			for (targetName = 0; targetName < nameArray.length; targetName += 1) {
				this.changeAllStatus(targetName, flag);
			}
			this.setState({
				houseAllChecked: flag,
				commerceAllChecked: flag,
				landAllChecked: flag,
				trafficAllChecked: flag,
				exceptraAllChecked: flag,
			});
		} else if (event.target.id === 'houseAllCheck') {
			const flag = !(this.state.houseAllChecked);
			this.setState({
				houseAllChecked: flag,
			});
			const targetIndex = 0;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'commerceAllCheck') {
			const flag = !(this.state.commerceAllChecked);
			this.setState({
				commerceAllChecked: flag,
			});
			const targetIndex = 1;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'landAllCheck') {
			const flag = !(this.state.landAllChecked);
			this.setState({
				landAllChecked: flag,
			});
			const targetIndex = 2;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'trafficAllCheck') {
			const flag = !(this.state.trafficAllChecked);
			this.setState({
				trafficAllChecked: flag,
			});
			const targetIndex = 3;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'exceptraAllCheck') {
			const flag = !(this.state.exceptraAllChecked);
			this.setState({
				exceptraAllChecked: flag,
			});
			const targetIndex = 4;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'ingFilterAllCheck') {
			const flag = !(this.state.ingFilterAllChecked);
			this.setState({
				ingFilterAllChecked: flag,
			});
			const targetIndex = 5;
			this.changeAllStatus(targetIndex, flag);
			if (this.state.ingFilter[2].checked === true) {
				this.focus.misnumActive = '';
			} else {
				this.focus.misnumActive = 'hide';
			}
		} else if (event.target.id === 'specialAllCheck') {
			const flag = !(this.state.specialAllChecked);
			this.setState({
				specialAllChecked: flag,
			});
			let targetIndex = 6;
			this.changeAllStatus(targetIndex, flag);
			targetIndex = 7;
			this.changeAllStatus(targetIndex, flag);
		} else if (event.target.id === 'procedureAllCheck') {
			const flag = !(this.state.procedureAllChecked);
			this.setState({
				procedureAllChecked: flag,
			});
			const targetIndex = 8;
			this.changeAllStatus(targetIndex, flag);
		}
	}
	// changeInput 예쁘게 다시 설계 필요
	changeInput(event) {
		event.preventDefault();
		this.setState({
			[event.target.id]: event.target.value,
		});
		if (event.target.name === 'start') {
			if (event.target.id === 'Ing') {
				this.args.misstart = event.target.value;
			} else if (event.target.id === 'App') {
				this.args.appstart = event.target.value;
			} else if (event.target.id === 'Min') {
				this.args.minstart = event.target.value;
			} else if (event.target.id === 'plot') {
				this.args.plotstart = event.target.value;
			} else if (event.target.id === 'area') {
				this.args.areastart = event.target.value;
			} else if (event.target.id === 'num') {
				this.args.numstart = event.target.value;
			}
		}
		if (event.target.name === 'end') {
			if (event.target.id === 'Ing') {
				this.args.misend = event.target.value;
			} else if (event.target.id === 'App') {
				this.args.append = event.target.value;
			} else if (event.target.id === 'Min') {
				this.args.minend = event.target.value;
			} else if (event.target.id === 'plot') {
				this.args.plotend = event.target.value;
			} else if (event.target.id === 'area') {
				this.args.areaend = event.target.value;
			} else if (event.target.id === 'num') {
				this.args.numend = event.target.value;
			}
		}
	}
	changeDayPick(event) {
		event.preventDefault();
		const day = event.target.value;
		if (event.target.id === 'startday') {
			this.setState({
				startDate: day,
				selectStartDay: new Date(day),
			});
		} else if (event.target.id === 'endday') {
			this.setState({
				endDate: day,
				selectEndDay: new Date(day),
			});
		}
	}
	changeCheckState = (targetName, index) => {
		const type = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
		const stateCopy = this.state[targetName];
		if (type.includes(targetName)) {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					kindAllChecked: false,
				});
				if (targetName === 'house') {
					this.setState({ houseAllChecked: false });
				} else if (targetName === 'commerce') {
					this.setState({ commerceAllChecked: false });
				} else if (targetName === 'land') {
					this.setState({ landAllChecked: false });
				} else if (targetName === 'traffic') {
					this.setState({ trafficAllChecked: false });
				} else if (targetName === 'exceptra') {
					this.setState({ exceptraAllChecked: false });
				}
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'ingFilter') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					ingFilterAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'special' || targetName === 'specialadd') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					specialAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'procedure') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					procedureAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		}
	}
	handleCheck(event) {
		const targetName = event.target.name;
		const index = event.target.value;
		this.changeCheckState(targetName, index);
		if (event.target.id === 'miscarriage') {
			if (this.state[targetName][index].checked === true) {
				this.focus.misnumActive = '';
			} else {
				this.focus.misnumActive = 'hide';
			}
		}
	}
	formatDate = (date) => {
		return (`${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`);
	}
	handleStartDate(date) {
		const startDay = this.formatDate(date);
		this.setState({
			selectStartDay: date,
			startDate: startDay,
		});
		document.getElementById('startday').value = startDay;
	}
	handleEndDate(date) {
		const endDay = this.formatDate(date);
		this.setState({
			selectEndDay: date,
			endDate: endDay,
		});
		document.getElementById('endday').value = endDay;
	}
	selectChange(event) {
		this.setState({
			numselect: event.target.value,
		});
	}
	filterEvent(event) {
		if (event.target.id === 'kind') {
			this.setState({
				kindState: !this.state.kindState,
				ingState: false,
				dateState: false,
				appraiseState: false,
				minState: false,
				addfilterState: false,
			});
		} else if (event.target.id === 'Ing') {
			this.setState({
				kindState: false,
				ingState: !this.state.ingState,
				dateState: false,
				appraiseState: false,
				minState: false,
				addfilterState: false,
			});
		} else if (event.target.id === 'Date') {
			this.setState({
				kindState: false,
				ingState: false,
				dateState: !this.state.dateState,
				appraiseState: false,
				minState: false,
				addfilterState: false,
			});
		} else if (event.target.id === 'App') {
			this.setState({
				kindState: false,
				ingState: false,
				dateState: false,
				appraiseState: !this.state.appraiseState,
				minState: false,
				addfilterState: false,
			});
		} else if (event.target.id === 'Min') {
			this.setState({
				kindState: false,
				ingState: false,
				dateState: false,
				appraiseState: false,
				minState: !this.state.minState,
				addfilterState: false,
			});
		} else if (event.target.id === 'Add') {
			this.setState({
				kindState: false,
				ingState: false,
				dateState: false,
				appraiseState: false,
				minState: false,
				addfilterState: !this.state.addfilterState,
			});
		}
	}
	changeFilterState(id) {
		console.log(id);
		const str = `${id.toLowerCase()}State`;
		console.log(str);
		if (id === 'App') {
			this.setState({
				appraiseState: false,
			})
		} else if (id === 'Add') {
			this.setState({
				addfilterState: false,
			})
		} else {
			this.setState({
				[str]: false,
			})
		}
	}
	filterSubmit(event) {
		this.changeFilterState(event.target.id);
        console.log('Filter Submit');
        this.args.query = this.props.query;
        console.log('Q : ', this.args.query);
        if (event.target.id === 'kind') {
			let targetName = 0;
			let index = 0;
			let addStr = '';
			this.focus.kindCount = 0;
			const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
			for (targetName = 0; targetName < nameArray.length; targetName += 1) {
				for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
					if (this.state[nameArray[targetName]][index].checked === true) {
						addStr = `${this.state[nameArray[targetName]][index].id},`;
						this.str += addStr;
						this.focus.kindCount += 1;
					}
				}
			}
			this.args.hq = this.str;
			this.props.onSearch(this.args);
			this.str = '';
			if (this.focus.kindCount > 0) {
				this.focus.kindActive = '';
				this.focus.kindColorActive = 'color';
			}
		} else if (event.target.id === 'Ing') {
			let index = 0;
			let addStr = '';
			this.focus.ingCount = 0;
			for (index = 0; index < this.state.ingFilter.length; index += 1) {
				if (this.state.ingFilter[index].checked === true) {
					addStr = `${this.state.ingFilter[index].id},`;
					this.str += addStr;
					this.focus.ingCount += 1;
				}
			}
			this.args.msq = this.str;
			this.args.mis = this.focus.misnumActive === 'hide' ? '' : `${this.args.misstart},${this.args.misend}`;
			this.props.onSearch(this.args);
			this.str = '';
			if (this.focus.ingCount > 0) {
				this.focus.ingActive = '';
				this.focus.ingColorActive = 'color';
			}
		} else if (event.target.id === 'Date') {
			this.args.date = `${this.state.startDate},${this.state.endDate}`;
			this.props.onSearch(this.args);
			if (this.state.selectStartDay !== '' || this.state.selectEndDay !== '') {
				this.focus.dateActive = '';
				this.focus.dateColorActive = 'color';
			}
		} else if (event.target.id === 'App') {
			this.args.app = `${this.args.appstart},${this.args.append}`;
			// this.args.app = this.args.app === ',' ? '' : this.args.app;
			this.props.onSearch(this.args);
			if (this.args.appstart !== '' || this.args.append !== '') {
				this.focus.appActive = '';
				this.focus.appColorActive = 'color';
			}
		} else if (event.target.id === 'Min') {
			this.args.min = `${this.args.minstart},${this.args.minend}`;
            // this.args.min = this.args.min === ',' ? '' : this.args.min;
			this.props.onSearch(this.args);
			if (this.args.minstart !== '' || this.args.minend !== '') {
				this.focus.minActive = '';
				this.focus.minColorActive = 'color';
			}
		} else if (event.target.id === 'Add') {
			let targetName = 0;
			let index = 0;
			let addStr = '';
			this.focus.addCount = 0;
			const nameArray = ['special', 'specialadd', 'procedure'];
			for (targetName = 0; targetName < nameArray.length; targetName += 1) {
				for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
					if (this.state[nameArray[targetName]][index].checked === true) {
						addStr = `${this.state[nameArray[targetName]][index].id},`;
						this.str += addStr;
						this.focus.addCount += 1;
					}
				}
			}
			this.args.add = this.str;
			this.args.plot = `${this.args.plotstart},${this.args.plotend}`;
			this.args.area = `${this.args.areastart},${this.args.areaend}`;
			this.args.num = `${this.state.numselect},${this.args.numstart},${this.args.numend}`;
			this.props.onSearch(this.args);
			this.str = '';
			if (this.focus.addCount > 0 || this.args.plotstart !== '' || this.args.plotend !== '' || this.args.areastart !== '' || this.args.areaend !== '' || this.args.numstart !== '' || this.args.numend !== '') {
				this.focus.addActive = '';
				this.focus.addColorActive = 'color';
			}
		}
	}
	showAllSpecial = () => {
		if (this.state.addMoreState === false) {
			this.focus.addMoreActive = '';
			this.setState({
				addMoreState: false * this.state.addMoreState,
			});
		} else if (this.state.addMoreState === 1) {
			this.focus.addMoreActive = 'hide';
			this.setState({
				addMoreState: false * this.state.addMoreState,
			});
		}
	}
	render() {
		let filterActive = 'filter-detail filter-kind';
		let filterIngActive = 'filter-detail filter-current';
		let dateActive = 'filter-detail filter-date';
		let appraiseActive = 'filter-detail filter-appraisal-price';
		let minActive = 'filter-detail filter-appraisal-price';
		let addFilterActive = 'filter-detail filter-add';
        // console.log('Q : ', this.props.query);
		const MONTHS = [
			'1월',
			'2월',
			'3월',
			'4월',
			'5월',
			'6월',
			'7월',
			'8월',
			'9월',
			'10월',
			'11월',
			'12월',
		];
		const WEEKDAYS_SHORT = ['일', '월', '화', '수', '목', '금', '토'];
		if (this.state.kindState === false) {
			filterActive = 'filter-detail filter-kind hide';
		} else {
			filterActive = 'filter-detail filter-kind';
		}
		if (this.state.ingState === false) {
			filterIngActive = 'filter-detail filter-current hide';
		} else {
			filterIngActive = 'filter-detail filter-current';
		}
		if (this.state.dateState === false) {
			dateActive = 'filter-detail filter-date hide';
		} else {
			dateActive = 'filter-detail filter-date';
		}
		if (this.state.appraiseState === false) {
			appraiseActive = 'filter-detail filter-appraisal-price hide';
		} else {
			appraiseActive = 'filter-detail filter-appraisal-price';
		}
		if (this.state.minState === false) {
			minActive = 'filter-detail filter-appraisal-price hide';
		} else {
			minActive = 'filter-detail filter-appraisal-price';
		}
		if (this.state.addfilterState === false) {
			addFilterActive = 'filter-detail filter-add hide';
		} else {
			addFilterActive = 'filter-detail filter-add';
		}
		const houseItems = this.state.house.map((data, idx) => {
			const name = 'house';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const commerceItems = this.state.commerce.map((data, idx) => {
			const name = 'commerce';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const exceptraItems = this.state.exceptra.map((data, idx) => {
			const name = 'exceptra';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const landItems = this.state.land.map((data, idx) => {
			const name = 'land';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const trafficItems = this.state.traffic.map((data, idx) => {
			const name = 'traffic';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const ingFilterItems = this.state.ingFilter.map((data, idx) => {
			const name = 'ingFilter';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const specialItems = this.state.special.map((data, idx) => {
			const name = 'special';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const specialaddItems = this.state.specialadd.map((data, idx) => {
			const name = 'specialadd';
			return (
				<CheckBox id={data.id} idx={idx} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const procedureItems = this.state.procedure.map((data, idx) => {
			return (
				<fragment>
					<input type="checkbox" id={data.id} name="procedure" value={idx} onChange={this.handleCheck} checked={data.checked} />
					<label htmlFor={data.id}>{data.label}</label>
				</fragment>
			)
		})
		return (
			<div className="row filter-section clearfix">
				<ul>
					<li>
						<div className={filterActive}>
							<form className="react-form" name="allKind">
								<div className="filter-row">
									<CheckBox id="kindAllCheck" onChange={this.selectAllChange} checked={this.state.kindAllChecked} label="전체선택" />
								</div>
								<div className="filter-row">
									<h5>주거용</h5>
									<CheckBox id="houseAllCheck" onChange={this.selectAllChange} checked={this.state.houseAllChecked} label="주거용 전체선택" />
									<div className="input-group">
										{houseItems}
									</div>
								</div>
								<div className="filter-row">
									<h5>상업용 및 업무용</h5>
									<CheckBox id="commerceAllCheck" onChange={this.selectAllChange} checked={this.state.commerceAllChecked} label="상업용 및 업무용 전체선택" />
									<div className="input-group">
										{commerceItems}
									</div>
								</div>
								<div className="filter-row">
									<h5>토지</h5>
									<CheckBox id="landAllCheck" onChange={this.selectAllChange} checked={this.state.landAllChecked} label="토지 전체선택" />
									<div className="input-group">
										{landItems}
									</div>
								</div>
								<div className="filter-row">
									<h5>차량 및 선박</h5>
									<CheckBox id="trafficAllCheck" onChange={this.selectAllChange} checked={this.state.trafficAllChecked} label="차량 및 선박 전체선택" />
									<div className="input-group">
										{trafficItems}
									</div>
								</div>
								<div className="filter-row">
									<h5>기타</h5>
									<CheckBox id="exceptraAllCheck" onChange={this.selectAllChange} checked={this.state.exceptraAllChecked} label="기타 전체선택" />
									<div className="input-group">
										{exceptraItems}
									</div>
								</div>
							<div className="row btn-section clearfix">
								<button type="reset" className="btn reset" id="kind" onClick={this.filterInit}>초기화</button>
								<button type="button" className="btn apply" id="kind" onClick={this.filterSubmit}>적용</button>
							</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.kindColorActive}`} id="kind" onClick={this.filterEvent} >물건종류
						<span className={`${this.focus.kindActive} font-color-white`}>⠐ {this.focus.kindCount}</span>
						</button>
					</li>
					<li>
						<div className={filterIngActive}>
							<form className="react-form">
								<div className="filter-row">
									<CheckBox id="ingFilterAllCheck" onChange={this.selectAllChange} checked={this.state.ingFilterAllChecked} label="전체선택" />
								</div>
								<div className="filter-row">
									<div className="input-group">
										{ingFilterItems}
									</div>
								</div>
								<div className={`filter-row ${this.focus.misnumActive}`}>
									<h5>유찰</h5>
									<input type="text" className="rd-form" name="start" id="Ing" onChange={this.changeInput} />
									<span> ~ </span>
									<input type="text" className="rd-form" name="end" id="Ing" onChange={this.changeInput} />
									<span> 회 </span>
								</div>
							<div className="row btn-section clearfix">
								<button type="reset" className="btn reset" id="Ing" onClick={this.filterInit}>초기화</button>
								<button type="button" className="btn apply" id="Ing" onClick={this.filterSubmit}>적용</button>
							</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.ingColorActive}`} id="Ing" onClick={this.filterEvent}>물건현황
							<span className={`${this.focus.ingActive} font-color-white`}>⠐ {this.focus.ingCount}</span>
						</button>
					</li>
					<li>
						<div className={dateActive}>
							<form className="react-form">
								<div className="filter-row">
									<table>
										<tr>
											<td>
												<input type="text" id="startday" className="rd-form text-center" placeholder={this.formatDate(new Date())} onBlur={this.changeDayPick} />
											</td>
											<td>
												<input type="text" id="endday" className="rd-form text-center" placeholder={this.formatDate(new Date())} onBlur={this.changeDayPick} />
											</td>
										</tr>
										<br />
										<tr>
											<td>
												<DayPicker months={MONTHS} weekdaysShort={WEEKDAYS_SHORT} onDayClick={this.handleStartDate} selectedDays={this.state.selectStartDay} />
											</td>
											<td>
												<DayPicker months={MONTHS} weekdaysShort={WEEKDAYS_SHORT} onDayClick={this.handleEndDate} selectedDays={this.state.selectEndDay} />
											</td>
										</tr>
									</table>
								</div>
								<div className="row btn-section clearfix">
									<button type="reset" className="btn reset" id="Date" onClick={this.filterInit}>초기화</button>
									<button type="button" className="btn apply" id="Date" onClick={this.filterSubmit}>적용</button>
								</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.dateColorActive}`} id="Date" onClick={this.filterEvent}>매각기일
							<span className={`${this.focus.dateActive} font-color-white`}>⠐ {this.state.startDate < 0 ? '' : this.state.startDate} - {this.state.endDate < 0 ? '' : this.state.endDate}</span>
						</button>
					</li>
					<li>
						<div className={appraiseActive}>
							<form className="react-form">
								<div className="filter-row">
									<h5>감정가</h5>
									<input type="text" className="rd-form" name="start" id="App" onChange={this.changeInput} />원
									~
									<input type="text" className="rd-form" name="end" id="App" onChange={this.changeInput} />원
								</div>
								<div className="row btn-section clearfix">
									<button type="reset" className="btn reset" id="App" onClick={this.filterInit}>초기화</button>
									<button type="button" className="btn apply" id="App" onClick={this.filterSubmit}>적용</button>
								</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.appColorActive}`} id="App" onClick={this.filterEvent}>감정가
							<span className={`${this.focus.appActive} font-color-white`}>⠐ {this.args.appstart < 0 ? '' : this.args.appstart} - {this.args.append < 0 ? '' : this.args.append}</span>
						</button>
					</li>
					<li>
						<div className={minActive}>
							<form className="react-form">
								<div className="filter-row">
									<h5>최저가</h5>
									<input type="text" className="rd-form" name="start" id="Min" onChange={this.changeInput} />원
									~
									<input type="text" className="rd-form" name="end" id="Min" onChange={this.changeInput} />원
								</div>
								<div className="row btn-section clearfix">
									<button type="reset" className="btn reset" id="Min" onClick={this.filterInit}>초기화</button>
									<button type="button" className="btn apply" id="Min" onClick={this.filterSubmit}>적용</button>
								</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.minColorActive}`} id="Min" onClick={this.filterEvent}>최저가
							<span className={`${this.focus.minActive} font-color-white`}>⠐ {this.args.minstart < 0 ? '' : this.args.minstart} - {this.args.minsend < 0 ? '' : this.args.minend}</span>
						</button>
					</li>
					<li>
						<div className={addFilterActive}>
							<form className="react-form">
								<div className="filter-row">
									<h5>대지면적</h5>
									<input type="text" className="rd-form" name="start" id="plot" onChange={this.changeInput} />㎡
									~
									<input type="text" className="rd-form" name="end" id="plot" onChange={this.changeInput} />㎡
								</div>
								<div className="filter-row">
									<h5>건물면적</h5>
									<input type="text" className="rd-form" name="start" id="area" onChange={this.changeInput} />㎡
									~
									<input type="text" className="rd-form" name="end" id="area" onChange={this.changeInput} />㎡
								</div>
								<div className="filter-row">
									<h5>지번범위</h5>
										<select className="rd-form select-dropdown" onChange={this.selectChange}>
											<option value="non-select">선택안함</option>
											<option value="all">전체</option>
											<option value="general">일반</option>
											<option value="mountain">산</option>
										</select>
									<input type="text" className="rd-form" name="start" id="num" onChange={this.changeInput} />
									~
									<input type="text" className="rd-form" name="end" id="num" onChange={this.changeInput} />
								</div>
								<div className="filter-row">
									<h5>특수물건</h5>
									<CheckBox id="specialAllCheck" onChange={this.selectAllChange} checked={this.state.specialAllChecked} label="특수물건 전체선택" />
									<div className="input-group">
										{specialItems}
									</div>
									<div>
										<button type="button" className="add-filter-more" onClick={this.showAllSpecial}>특수물건 모두 보기</button>
										<div className={this.focus.addMoreActive}>
											{specialaddItems}
										</div>
									</div>
								</div>
								<div className="filter-row">
									<h5>경매절차</h5>
									<CheckBox id="procedureAllCheck" onChange={this.selectAllChange} checked={this.state.procedureAllChecked} label="경매절차 전체선택" />
									<div className="input-group">
										{procedureItems}
									</div>
								</div>
								<div className="row btn-section clearfix">
									<button type="reset" className="btn reset" id="Add" onClick={this.filterInit}>초기화</button>
									<button type="button" className="btn apply" id="Add" onClick={this.filterSubmit}>적용</button>
								</div>
							</form>
						</div>
						<button type="button" className={`btn filter active ${this.focus.addColorActive}`} id="Add" onClick={this.filterEvent}>필터 추가하기
							<span className={`${this.focus.addActive} font-color-white`}>⠐ {this.focus.addCount}</span>
						</button>
					</li>
				</ul>
			</div>
		);
	}
}
FilterHeader.propTypes = {
	// status: PropTypes.bool.isRequired,
	onSearch: PropTypes.func.isRequired,
    query: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
    query: state.listBox.query,
});

FilterHeader = connect(mapStateToProps)(FilterHeader);

export default FilterHeader;
