/* eslint-disable prefer-arrow-callback,space-before-function-paren,quotes,import/no-duplicates */
// import { connect } from 'react-redux';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import sample1 from '../assets/images/sample01.jpg';
// import connect from 'react-redux/es/connect/connect';

// let mapObject = null;
class DaumMapList extends Component {
	constructor(props) {
		super(props);
		// console.log('DaumMap ConStructor!');
		this.state = {
			disAlert: 'show',
			// mapLevel: 7,
		};
	}
	myX = 0;
	myY = 0;
	markerNum = 0;
	shouldComponentUpdate(nextProps, nextState) {
		// console.log('shouldComponentUpdate: ', JSON.stringify(nextProps), ' ', JSON.stringify(this.props));
		return ((JSON.stringify(nextProps) !== JSON.stringify(this.props)) || (JSON.stringify(nextState) !== JSON.stringify(this.state)));
	}
	componentDidMount() {
		this.daumMap = window.daum.maps;
		const el = document.getElementById('map');
		const level = 7;
		const centerLat = 37.4981646;
		const centerLng = 127.028307;
		// const centerLat = this.myX / this.markerNum;
		// const centerLng = this.myY / this.markerNum;
		const zoomControl = new this.daumMap.ZoomControl();
		this.obj = this;
		// mapObject = this;
		this.markers = [];
		this.contents = [];
		this.infoMarker = [];
		this.infoContents = [];
		const map = new this.daumMap.Map(el, {
			center: new this.daumMap.LatLng(centerLat, centerLng),
			level: level,
		});
		// console.log(centerLat, centerLng)
		const clusterer = new this.daumMap.MarkerClusterer({
			map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
			averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
			minLevel: 7, // 클러스터 할 최소 지도 레벨
			markers: this.markers,
			gridSize: 60,
			clickable: true, // 클릭할 수 있음
			disableClickZoom: true, // 클러스터 클릭 시 지도 확대 되지 않는다
			styles: [{
				width: '43px',
				height: '42px',
				background: '#1e47b0 no-repeat',
				borderRadius: '100%',
				color: '#fff',
				textAlign: 'center',
				lineHeight:'43px',
				fontSize: '15px',
				opacity: 0.87,
			}],
		});
		this.map = map;
		this.map.setMinLevel(3);
		this.clusterer = clusterer;
		this.clusterer.setMinClusterSize(1);
		map.addControl(zoomControl, this.daumMap.ControlPosition.RIGHT);
		this.args = this.getLatLng();
		// console.log("DaumMapList : ", this.args);
		// console.log(this.props);
		this.props.onChange(this.args);
		// 마커 클러스터 클릭 시 이벤트 발생
		this.daumMap.event.addListener(clusterer, 'clusterclick', (cluster) => {
			const clusteringLevel = this.map.getLevel() - 1;
			this.map.setLevel(clusteringLevel, { anchor: cluster.getCenter() });
		});

		// 클러스터링 끝난 후 실행
		this.daumMap.event.addListener(clusterer, 'clustered', () => {
			// console 사용하려면 위에 () 안에 cluster를 넣어야 합니다! 잊지 말기.
			// console.log('clustered', cluster);
		});

		// 드래그가 끝날 때 이벤트 발생
		this.daumMap.event.addListener(map, 'dragend', () => {
			this.removeMarker();
			this.removeInfoMarker();
			this.clusterer.clear();
			this.args = this.getLatLng();
			this.props.onChange(this.args);
		});
		// 확대 수준이 변경될 때 이벤트 발생
		this.daumMap.event.addListener(map, 'zoom_changed', () => {
			this.removeMarker();
			this.removeInfoMarker();
			this.clusterer.clear();
			this.args = this.getLatLng();
			this.props.onChange(this.args);
			// const mapLevel = this.map.getLevel();
			this.setState({
				// mapLevel: mapLevel,
			});
		});
		// 지도 클릭 시 이벤트 발생
		this.daumMap.event.addListener(map, 'click', () => {
			this.removeInfoMarker();
		});
	}
	onMouseDown(item) {
		const object = item.target.parentElement;
		// console.log(object.className);
		if (object.classList.contains('goto_')) {
			// console.log(object.getAttribute('record-id'));
			// 이전 상태로 돌아감
			this.props.history.push(`/post/${object.getAttribute('record-id')}`);
		}
	}
	addEventHandle(target, type, callback) {
		// target node에 이벤트 핸들러를 등록하는 함수입니다
		if (target.addEventListener) {
			target.addEventListener(type, callback);
		} else {
			target.attachEvent(`on${type}`, callback);
		}
	}
	removeEventHandle(target, type, callback) {
		// target node에 등록된 이벤트 핸들러를 제거하는 함수입니다
		if (target.removeEventListener) {
			target.removeEventListener(type, callback);
		} else {
			target.detachEvent(`on${type}`, callback);
		}
	}
	// marker가 지워지는 경우는 map의 범위에서 벗어났을 때이다.
	// mouseDown일 때 removeHandler가 동작하고
	// marker array와 contents array가 비워진다.
	removeMarker() {
		if (this.markers) {
			if (this.markers.length > 0) {
				for (let i = 0; i < this.markers.length; i += 1) {
					// setMap을 null로 지정할 시, 마커가 없어진다.
					this.markers[i].setMap(null);
					this.removeEventHandle(this.contents[i], 'mousemove', this.onMouseDown);
				}
				this.markers = [];
				this.contents = [];
			}
		}
	}
	removeInfoMarker() {
		if (this.infoMarker) {
			if (this.infoMarker.length > 0) {
				for (let i = 0; i < this.infoMarker.length; i += 1) {
					// setMap을 null로 지정할 시, 마커가 없어진다.
					this.infoMarker[i].setMap(null);
					this.removeEventHandle(this.infoContents[i], 'click', this.onMouseDown);
				}
				this.infoMarker = [];
			}
		}
	}
	getLatLng() {
		// const extraLat = 0.025;
		// const extraLng = 0.005;
		const subBounds = this.map.getBounds();
		// 영역의 중심 좌표를 얻어옵니다.
		const center = this.map.getCenter();
		// 영역의 남서쪽 좌표를 얻어옵니다
		const swLatLng = subBounds.getSouthWest();
		// 영역의 북동쪽 좌표를 얻어옵니다
		const neLatLng = subBounds.getNorthEast();
		const message = `지도의 남서쪽 좌표는 ${swLatLng.getLat()}, ${swLatLng.getLng()} 이고 <br>
		북동쪽 좌표는 ${neLatLng.getLat()}, ${neLatLng.getLng()} 입니다`;
		const args = {
			center: center,
			sw_lat: swLatLng.getLat(), // + extraLat,
			sw_lng: swLatLng.getLng(), // - extraLng,
			ne_lat: neLatLng.getLat(), // - extraLat,
			ne_lng: neLatLng.getLng(), // + extraLng,
			message: message,
		};
		// console.log('------------------coords-----------------', swLatLng.getLat(), swLatLng.getLng(), neLatLng.getLat(), neLatLng.getLng());
		this.centerLat = (args.sw_lat + args.ne_lat) / 2;
		this.centerLng = (args.sw_lng + args.ne_lng) / 2;
		return args;
	}
	// count = 0
	addMarker(row) {
		// this.count = this.count + 1;
		// console.log('row : ', this.count, row);
		const content = document.createElement('div');
		const minPrice = row.record_price[2] ? this.convertToPrice(row.record_price[2]) : '정보 없음';
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		content.innerHTML = `
						<div class="list-marker">
							<div class="kind">
								<span class="kind">${row.record_type}</span>
							</div>
							<div class="col-4 lowest">
								최저
							</div>
							<div class="col-8 price">
								${minPrice}
							</div>
						</div>`;
		// 커스텀 오버레이가 표시될 위치입니다
		const po = new this.daumMap.LatLng(row.lat, row.lng);
		// 커스텀 오버레이를 생성합니다
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: po,
			content: content,
			clickable: true,
		});

		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		content.addEventListener('click', () => {
			this.removeInfoMarker();
			this.map.panTo(po);
			const w = document.createElement('div');
			w.innerHTML = `<div class='overlay_info_post col col-3 post' style='width: 200px;'>
							<div class='list' style='margin-bottom: 0px;'>
								<div class='list-thumbnail goto_' record-id=${row.ID}>
									<img src=${row.record_thumb ? row.record_thumb : sample1} class='img-fluid' alt='이미지'>
									<span class='dday-bx absolute'>
										<em>${row.record_gil_dday < 0 ? `D${row.record_gil_dday}` : row.record_MulGeonStatus}</em>
									</span>
									<span class='like absolute hide'>
										<i class='mdi mdi-heart'></i>
									</span>
								</div><div class='list-meta goto_' record-id=${row.ID}><div class='title'>사건번호 ${row.ID}
										</div>
							<div class='address'>[${row.record_type}] ${row.record_address}</div>
							<div class='row price-section clearfix'>
								<div class='col-3 appraise'><span class='badge'>감정가</span></div>
								<div class='col-9'><div class='appraise-price price'>${row.record_Appraisal_price}
								</div>
							</div>
						</div>
						<div class='row price-section clearfix'>
							<div class='col-3 lowest'>
								<span class='badge'>최저가</span>
							</div>
							<div class='col-9'><div class='lowest-price price'>
								<span>${row.record_Appraisal_min_price ? row.record_Appraisal_min_price : '최저가 정보 없음'}
								</span>
							</div>
						</div></div></div></div></div>`;
			// removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다
			const iwRemoveable = true;
			// 커스텀 오버레이를 생성합니다
			// const newPo = new this.daumMap.LatLng(this.centerLat, this.centerLng);
			const infoMarkerOverlay = new this.daumMap.CustomOverlay({
				ZIndex: 5000,
				position: po,
				content: w,
				removable: iwRemoveable,
			});
			infoMarkerOverlay.setMap(this.map);
			this.infoMarker.push(infoMarkerOverlay);
			this.infoContents.push(w);
			w.addEventListener('click', () => {
					this.props.history.push(`/post/${row.ID}`);
				});
		});
	}
	disAlertHide = () => {
		this.setState({
			disAlert: 'hide',
		});
	};
	convertToPrice(price) {
		let minPriceStr = '';
		const intPriceHundred = Math.floor(price / 1000000) % 10;
		const intPriceThousand = Math.floor(price / 10000000) % 10;
		const intPriceHundredMillion = Math.floor(price / 100000000);
		const chun = intPriceThousand === 0 ? '' : `${intPriceThousand}천`;
		const baek = intPriceHundred === 0 ? '' : `${intPriceHundred}백`;
		minPriceStr = price >= 100000000 ? `${intPriceHundredMillion}억 ${chun}` : `${chun} ${baek}`;
		return minPriceStr;
	}
	render() {
		// console.log("DaumMap");
		if (this.daumMap) {
			if (this.props.lat !== 0 && this.props.lng !== 0) {
				const coords = new this.daumMap.LatLng(this.props.lat, this.props.lng);
				this.map.setCenter(coords);
				this.args = this.getLatLng();
				this.props.onChange(this.args);
			} else {
				// 가져온 여러 데이터들을 하나씩 addMarker로 처리한다.
				// 이보다는 markers에 하나씩 저장하여 addMarker를 마지막에 한 번 처리하는 것이 어떨까
				this.props.records.forEach((item) => {
					if (item.lat && item.lng) {
						this.myX += item.lat;
						this.myY += item.lng;
						this.markerNum = this.markerNum + 1;
					}
					// console.log(item.lat, item.lng);
					// console.log(this.myX, this.myY, this.markerNum);
				})
				const centerLat = this.myX / this.markerNum;
				const centerLng = this.myY / this.markerNum;
				const coords = new this.daumMap.LatLng(centerLat, centerLng);
				this.myX = 0;
				this.myY = 0;
				this.markerNum = 0;

				this.map.setCenter(coords);
				this.markers = [];
				this.clusterer.clear();
				this.props.records.forEach((row) => {
					// console.info('record row!', row);
					this.addMarker(row);
				});
				// List에 표시될 24개의 record만 가져온다.
				this.clusterer.addMarkers(this.markers);
				console.log(this.clusterer);
			}
		}
		return (
			<div className="col-4 map-section">
			<div className="App map" id="map" />
			<div className="App map" id="map" />
			<div className={`alert alert-dismissible opacity-light fade ${this.state.disAlert}`}>
				지도를 드래그하여 다른 매물을 볼 수 있습니다.
				<button type="button" className="close" aria-label="Close" onClick={this.disAlertHide}>
					<span><i className="mdi mdi-close" /></span>
				</button>
			</div>
	</div>
		);
	}
}

DaumMapList.propTypes = {
	lat: PropTypes.number.isRequired,
	lng: PropTypes.number.isRequired,
	records: PropTypes.array.isRequired,
	// foundRecord: PropTypes.number.isRequired,
	// currentPage: PropTypes.number.isRequired,
	onChange: PropTypes.func.isRequired,
	history: PropTypes.func.isRequired,
	// recordPerPage: PropTypes.number.isRequired,
};

// 스토어 안의 state 값을 props로 연결합니다.
// const mapStateToProps = state => ({
//     query: state.query,
// });
//
// DaumMapList = connect(mapStateToProps)(DaumMapList);

// export default DaumMap;
export default withRouter(DaumMapList);
