import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PopupScreen extends Component {
	render() {
		return (
			<button className={`popup-btn-bx ${this.props.display}`} onClick={this.props.handlePopupType} name={this.props.name} value={this.props.value}>{this.props.label}</button>
		);
	}
}

PopupScreen.propTypes = {
	display: PropTypes.string.isRequired,
	handlePopupType: PropTypes.func.isRequired,
	name: PropTypes.number.isRequired,
	value: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
};

export default PopupScreen;
