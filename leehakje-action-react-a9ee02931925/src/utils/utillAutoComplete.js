import axios from 'axios';
import axiosCancel from 'axios-cancel';

let requestId4 = '';
// let vars = [];

axiosCancel(axios, {
    debug: false,
});

async function getAutoComplete(keyword) {
    if (requestId4) {
        axios.cancel(requestId4);
    }
    requestId4 = 'request4';

    try {
        const params = {
            keyword: keyword,
        };
        const response = await axios.get('http://localhost:8000/search/autocomplete', {
            // const response = await axios.get('http://127.0.0.1:8000/search', {
            headers: {
                'Content-Type': 'application/json',
            },
            params: params,
        });
        // vars = params;
        // console.log('utilSearchRcord', response.data);
        return response;
    } catch (error) {
        return [];
    }
}


export default getAutoComplete;
