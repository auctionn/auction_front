import axios from 'axios';
import axiosCancel from 'axios-cancel';


let requestId4 = '';

axiosCancel(axios, {
    debug: false,
});

async function getSearch(keyword) {
    if (requestId4) {
        axios.cancel(requestId4);
    }
    requestId4 = 'request4';

    try {
        const params = {
            keyword: keyword,
        };
        // const response = await axios.get('http://13.209.76.116/search_q_record.php', {
            const response = await axios.get('http://127.0.0.1:8000/search/', {
            requestId: requestId4,
            headers: {
                'Content-Type': 'application/json',
            },
            params: params,
        });
        // vars = params;
        // console.log('utilSearchRcord', response.data);
        return response.data;
    } catch (error) {
        console.log(error)
        return [];
    }
}

export default getSearch;
